package app.numair.qadir.surveys.nimbl3.com.network;

import java.util.List;

import app.numair.qadir.surveys.nimbl3.com.bean.Survey;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Numair on 8/14/2016.
 */
public interface ApiService {

    /*
    Retrofit get annotation with our URL
    And our method that will return us the List of ContactList
    */
    @GET("/app/surveys.json?access_token=" + Urls.API_TOKEN)
    Call<List<Survey>> getLatestSurveyList();
}