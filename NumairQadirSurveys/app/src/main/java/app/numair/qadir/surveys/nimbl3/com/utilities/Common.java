package app.numair.qadir.surveys.nimbl3.com.utilities;

import android.content.Context;
import android.util.DisplayMetrics;
import android.util.TypedValue;

/**
 * Created by Numair on 8/13/2016.
 */
public class Common {
    // Font path
    public static final String fontPath = "fonts/font_black.ttf";

    // Large Image
    public static final String Large_Image = "_l";

    public static int getScreenHeight(Context ctx) {
        DisplayMetrics dm = ctx.getResources().getDisplayMetrics();
        return dm.heightPixels;
    }

    public static int dp2px(Context paramContext, float paramFloat) {
        DisplayMetrics localDisplayMetrics = paramContext.getResources()
                .getDisplayMetrics();
        return (int) TypedValue.applyDimension(1, paramFloat,
                localDisplayMetrics);
    }
}
