package app.numair.qadir.surveys.nimbl3.com.utilities;

import android.util.Log;

/**
 * Created by Numair on 8/14/2016.
 */
public class AppLog {

    private static final String APP_TAG = "Nimbl3-Survey";
    private static boolean isLogON = true;

    public static int Message(String message) {
        return isLogON ? Log.i(APP_TAG, message) : 0;
    }

    public static int Error(String message) {
        return isLogON ? Log.e(APP_TAG, message) : 0;
    }

    public static int Information(String message) {
        return isLogON ? Log.i(APP_TAG, message) : 0;
    }

    public static int Debug(String message) {
        return isLogON ? Log.d(APP_TAG, message) : 0;
    }

    public static int Error(String tag, String message) {
        return isLogON ? Log.e(tag, message) : 0;
    }

    public static int Information(String tag, String message) {
        return isLogON ? Log.i(tag, message) : 0;
    }

    public static int Debug(String tag, String message) {
        return isLogON ? Log.d(tag, message) : 0;
    }

    public static int StackDebug(String tag, String message) {
        return isLogON ? Log.d(tag + "_Stack", message) : 0;
    }
}
