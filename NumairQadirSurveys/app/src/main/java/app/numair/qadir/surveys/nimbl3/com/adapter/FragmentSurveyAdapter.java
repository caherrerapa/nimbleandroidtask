package app.numair.qadir.surveys.nimbl3.com.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;

import java.util.ArrayList;
import java.util.List;

import app.numair.qadir.surveys.nimbl3.com.bean.Survey;
import app.numair.qadir.surveys.nimbl3.com.fragment.FragmentSurvey;

public class FragmentSurveyAdapter extends FragmentPagerAdapter {
    private List<Survey> surveyList = new ArrayList<>();

    public FragmentSurveyAdapter(FragmentManager fm, List<Survey> list) {
        super(fm);
        this.surveyList = list;
    }

    @Override
    public Fragment getItem(int position) {
        return FragmentSurvey.newInstance(surveyList.get(position));
    }

    @Override
    public int getCount() {
        return surveyList.size();
    }

    @Override
    public int getItemPosition(Object object) {
//        return super.getItemPosition(object);
        return PagerAdapter.POSITION_NONE;
    }
}